rm -rf ../build/py-lambda # Remove existing
rsync -av --progress . ../build/py-lambda --exclude venv --exclude build.sh
cd ../build/py-lambda
pip3 install -r requirements.txt -t .