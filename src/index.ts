#!/usr/bin/env node
import 'source-map-support/register';
import * as cdk from '@aws-cdk/core';
import { RootStack } from './stacks/root_stack';
import PipelineStack from './stacks/pipeline_stack';

const app = new cdk.App();
new RootStack(app, 'LearnCDKDeploy');
new PipelineStack(app, "LearnCDKPipelineStack")
app.synth()