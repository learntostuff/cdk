import {LambdaRestApi} from '@aws-cdk/aws-apigateway';
import {Construct, Stack, StackProps} from '@aws-cdk/core';
import {Code, Function, Runtime} from "@aws-cdk/aws-lambda"

export class RootStack extends Stack {
  constructor(scope: Construct, id: string, props?: StackProps) {
    super(scope, id, props);
    const tsLambda = new Function(this, "ts-lambda", {
      runtime: Runtime.NODEJS_12_X,
      handler: "index.handler",
      code: Code.fromAsset('build/ts-lambda')
    })
    const pyLambda = new Function(this, "py-lambda", {
      runtime: Runtime.PYTHON_3_8,
      handler: "main.handler",
      code: Code.fromAsset('build/py-lambda')
    })
    const api = new LambdaRestApi(this, "Gateway", {
      handler: tsLambda
    })
  }
}
