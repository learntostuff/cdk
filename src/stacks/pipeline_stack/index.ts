import { Artifact } from "@aws-cdk/aws-codepipeline";
import { BitBucketSourceAction } from "@aws-cdk/aws-codepipeline-actions";
import { Construct, Stack, StackProps, Stage, StageProps } from "@aws-cdk/core";
import { CdkPipeline, SimpleSynthAction } from "@aws-cdk/pipelines";
import {RootStack} from '../root_stack'

class LearnCDKStage extends Stage {
  constructor(scope: Construct, id: string, props?: StageProps){
    super(scope, id, props)
    const rootStack = new RootStack(this, "LearnCDKRootStack")
  }
}

export default class PipelineStack extends Stack {
  constructor(scope: Construct, id: string, props?: StackProps){
    super(scope, id, props)

    const sourceArtifact = new Artifact()
    const cloudAssemblyArtifact = new Artifact()

    const sourceAction = new BitBucketSourceAction({
      actionName: "BitbucketSourceAction",
      connectionArn: "arn:aws:codestar-connections:us-east-1:412556261185:connection/3ceaa851-b15e-479a-8b1e-a0f005ea9770",
      output: sourceArtifact,
      owner: "learntostuff",
      repo: "cdk"
    })

    const pipeline = new CdkPipeline(this, "pipeline", {
      sourceAction,
      synthAction: SimpleSynthAction.standardNpmSynth({
        sourceArtifact,
        cloudAssemblyArtifact,
        buildCommand: "npm run build",
      }),
      cloudAssemblyArtifact
    })

    pipeline.addApplicationStage(new LearnCDKStage(this, "LearnCDKStage-Prod"))
  }
}