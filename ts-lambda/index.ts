import Axios from "axios"

export async function handler(event: Record<any, any>){
  const response = await Axios.get("https://api.github.com/zen")
  return {
    statusCode: "200",
    body: JSON.stringify({
      message: response.data
    })
  }
}